
describe('Tickets', () => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"))

    it('Preenche todos os campos de texto', () => {
        const firstName = "Gabriella"
        const lastName = "Reis"

        cy.get("#first-name").type(firstName)
        cy.get('#last-name').type(lastName)
        cy.get('#email').type('test@test.com')
        cy.get('#requests').type('vegetarian')
        cy.get('#signature').type(`${firstName} ${lastName}`)
    })

    it('Seleciona dois tickets', () => {
        cy.get('#ticket-quantity').select("2")
    })

    it('Seleciona ticket tipo vip', () => {
        cy.get('#vip').check()
    })

    it('Seleciona checkbox "social media"', () => {
        cy.get('#social-media').check()
    })

    it('Seleciona "friend" e "publication" e uncheck "friend"', () => {
        cy.get('#friend').check()
        cy.get('#publication').check()

        cy.get('#friend').uncheck()
    })

    it('Deve ter o header "ticketbox"', () => {
        cy.get('header h1').should('contain', 'TICKETBOX')
    })

    it('Alerts com email inválido', () => {
        cy.get('#email')
            .as('email')
            .type('test-test.com')

        cy.get('#email.invalid').should('exist')

        cy.get('@email').type('test@tesst.com')
        cy.get('#email.invalid').should('not.exist')
    })

    it('Preencher e resetar o formulário', () => {
        const firstName = "Gabriella"
        const lastName = "Reis"
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName)
        cy.get('#last-name').type(lastName)
        cy.get('#email').type('test@test.com')
        cy.get('#ticket-quantity').select("2")
        cy.get('#vip').check()
        cy.get('#friend').check()
        cy.get('#requests').type('vegetarian')

        cy.get('.agreement p').should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets`)

        cy.get('#agree').click()
        cy.get('#signature').type(fullName)

        cy.get('button[type="submit"]')
            .as('submitButton')
            .should('not.be.disabled')

        cy.get('button[type="reset"]').click()
        cy.get('@submitButton').should('be.disabled')
    })

    it('Preenche apenas campos obrigatórios', () => {
        const costumer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@test.com"
        }

        cy.fillMandatoryfields(costumer)

        cy.get('button[type="submit"]')
            .as('submitButton')
            .should('not.be.disabled')

        cy.get('#agree').uncheck()
        cy.get('@submitButton').should('be.disabled')

    })

})

